package collections;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import selenium.BaseClass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.BackingStoreException;

public class Class2 extends BaseClass {
    /**
     * List Interface
     *
     * 1. List can accept duplicates
     * 2. LIst will maintain insertion order
     * 3. Size is growabel in nature
     * 4. List was implemented by three classes
     *    1. ArrayList
     *    2. LInked List
     *    3. Vector
     */

    @Test
    public void function1(){
        ArrayList arrayList = new ArrayList();


        arrayList.add("String");
        arrayList.add(null);
        arrayList.add(true);
        arrayList.add(7);
        arrayList.add(45.00);
        arrayList.add('c');


        byte b = 10;

        byte c =(byte)(b+1);

        for (Object obj : arrayList){
            Integer v = (Integer)obj;
        }
    }

    @Test
    public void function2(){
        String v = null;
        List<String> arrayList = new ArrayList();

        arrayList.add("Testing");
        arrayList.add("MURTHY");
        arrayList.add("Bhargavi");
        arrayList.add("Bhargavi");

        List<WebElement> value = remoteWebDriver.findElements(By.xpath(""));

        for (String values : arrayList){
            System.out.println(values);
        }
    }

    @Test
    public void function3(){
        String v = null;
        List<Integer> arrayList = new ArrayList();

        arrayList.add(10);
        arrayList.add(20);
        arrayList.add(30);
        arrayList.add(40);
        arrayList.add(50);
        arrayList.add(60);
        arrayList.add(70);

        arrayList.add(10);
        arrayList.add(2, 40);


        for (Integer a : arrayList){
            System.out.println(a);
        }
    }

    @Test
    public void function4(){
        String v = null;
        ArrayList<Integer> arrayList = new ArrayList<>(100);

        arrayList.add(10);
        arrayList.add(30);
        arrayList.add(50);
        arrayList.add(30);
        arrayList.add(10);
        arrayList.add(30);
        arrayList.add(50);
        arrayList.add(30);
        arrayList.add(10);
        arrayList.add(30);

        arrayList.add(50);



        for (Integer a : arrayList){
            System.out.println(a);
        }
    }


    @Test
    public void function(){
        String v = null;
        LinkedList<Integer> arrayList = new LinkedList<>();

        arrayList.add(10);
        arrayList.add(30);
        arrayList.add(50);
        arrayList.add(30);
        arrayList.add(10);
        arrayList.add(30);
        arrayList.add(50);
        arrayList.add(30);
        arrayList.add(10);
        arrayList.add(30);

        arrayList.add(50);
        arrayList.add(10, 300);



        for (Integer a : arrayList){
            System.out.println(a);
        }
    }
}

