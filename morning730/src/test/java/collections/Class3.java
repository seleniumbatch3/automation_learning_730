package collections;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Vector;

public class Class3 {

    @Test
    public void function1(){

        ArrayList<String> arrayList = new ArrayList<>(100);
        Vector<String> vector = new Vector<>();
        vector.add("Testing");
        arrayList.add("STring");
        arrayList.add("Kiran");
        arrayList.add("Teny");

        for ( String value : arrayList){

            String val = value;
            System.out.println(" values for Arraylist " + val);

        }

        ArrayList<Integer> arrayList1 = new ArrayList<>();
        arrayList1.add(10);
        arrayList1.add(20);
        arrayList1.add(30);

        for ( int a : arrayList1){
            System.out.println(" values for Arraylist1 " + a);
        }

    }


    @Test
    public void function2(){

        LinkedList<String> linkedList = new LinkedList();
        linkedList.add("STring");
        linkedList.add("Kiran");
        linkedList.add("Teny");

        for ( String value : linkedList){

            String val = value;
            System.out.println(" values for Arraylist " + val);

        }

        LinkedList<Integer> linkedList1 = new LinkedList<>();
        linkedList1.add(10);
        linkedList1.add(20);
        linkedList1.add(30);

        for ( int a : linkedList1){
            System.out.println(" values for Arraylist1 " + a);
        }

    }
}
