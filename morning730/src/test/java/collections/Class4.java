package collections;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.util.*;

public class Class4 {
    /**
     * Set
     * 1. Set does not allow duplicates
     * 2. Insertion order is not preserved
     * 3. It can accept both Homogenious and Heterogenious values
     * 4. Null insertion is possible only one time.
     */

    /**
     * HashSet
     */
    @Test
    public void m1(){

        HashSet<String> h = new HashSet<>();
        h.add("Testing");
        h.add("Murthy");
        h.add("Supraja");
        h.add("Sai");
        h.add("Murthy");
        h.add(null);
        h.add(null);

        System.out.println("Set values " + h);
    }

    /**
     * LInked HashSet
     */
    @Test
    public void m2(){
        LinkedHashSet<String> h = new LinkedHashSet();
        h.add("Testing");
        h.add("Murthy");
        h.add("Supraja");
        h.add("Sai");
        h.add("Murthy");
        h.add(null);
        h.add(null);

        System.out.println("Set values " + h);


    }

    @Test
    public void m(){
        TreeSet<String> h = new TreeSet<>();
        h.add("Testing");
        h.add("Murthy");
        h.add("Supraja");
        h.add("Sai");
        h.add("Murthy");

        System.out.println("TreeSet values " + h);


    }
}
