package collections;

import org.testng.annotations.Test;

import java.util.*;

public class Class5 {

    @Override
    public String toString() {
        return "MY CLASS5 Object @"+hashCode();
    }

    @Override
    public int hashCode() {
        return 456;
    }

    @Test
    public void m1(){

        HashMap<Integer, String> map= new HashMap();

        map.put(1, "Venkat");
        map.put(2, "Hyderabad");
        map.put(3, "Hyderabad");

        System.out.println(map.get(4));

    }

    @Test
    public void m2(){

        HashMap<Integer, String> map= new HashMap();

        System.out.println("map Object" + map);
        String s = new String();
        String s1 = "456789";
        System.out.println(s1.subSequence(1, 5).chars());

        map.put(1, "Venkat");
        map.put(2, "Hyderabad");
        map.put(3, "Hyderabad");

        for (int a : map.keySet()){
            System.out.println("Keys " + map.get(a));
        }

        Set  values = map.entrySet();
        Iterator val = values.iterator();
        while(val.hasNext()){
            Map.Entry value = (Map.Entry)(val.next());
            System.out.println(value.getKey());
        }

    }

    @Test
    public void m3(){
        HashMap<Integer, String> map= new HashMap();
        map.put(1, "Venkat");
        map.put(2, "Hyderabad");
        map.put(3, "Hyderabad");
        System.out.println("map Object" + map);

        Class5 class5 = new Class5();
        System.out.println("printing class5 Object " + class5);

        HashMap<String, Map<String, List<String>>> ma = new HashMap<>();
        ArrayList<String> al = new ArrayList<>();
        al.add("VENKAT");
        al.add("Supraja");

        HashMap<String, List<String>> map1 = new HashMap<>();
        map1.put("Testing", al);

        ma.put("parentMap", map1);

        System.out.println("Printing parent Map object " + ma.get("parentMap"));
    }

    @Test
    public void m4(){
        HashMap<Integer, String> map= new HashMap();
        map.put(1, "Venkat");
        map.put(2, "Hyderabad");
        map.put(3, "Hyderabad");
        map.put(null, "Hyderabad");
        map.put(null, "Vizag");
        System.out.println("map Object" + map);


//        Hashtable<Integer, String> hashtable = new Hashtable<>();
//        hashtable.put(1, "Venkat");
//        hashtable.put(2, "Hyderabad");
//        hashtable.put(3, "Hyderabad");
//        hashtable.put(null, "Hyderabad");
//        hashtable.put(null, "Vizag");
//
//        System.out.println("HashTable" + hashtable);
    }
}
