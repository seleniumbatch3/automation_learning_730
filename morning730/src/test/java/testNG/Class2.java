package testNG;

import org.testng.annotations.*;

public class Class2 {



    @Test
    @Parameters({"browserName", "url"})
    public void validatePageTitle(String browserName, String url)
    {
        System.out.println("Browser name =====" + browserName + " " + url);
    }

    @Test(description = "This talks about something",
            groups = {"UI", "regression"},
            priority = 1,
            enabled = false,
            dependsOnMethods = "test", dataProvider = "test")
    @Parameters({"browserName", "url"})
    public void validatePageHeader()
    {
        System.out.println("UI Validation");
    }

    @Test(groups = {"UI", "sanity"})
    public void validateCloseButton(){
        System.out.println("Do Validation");
    }

}
