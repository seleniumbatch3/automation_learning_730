package testNG;

public class FunctionsClass {

    public int sumOfTwoNumber(int a, int b){
        return a+b;
    }

    public int subtractTwoNumber(int a, int b){
        return a-b;
    }

    public int multiplicationOfTwoNumber(int a, int b){
        return a*b;
    }

    public int divisionOfTwoNumber(int a, int b){
        return a/b;
    }

}
