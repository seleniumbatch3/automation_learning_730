package testNG;

import org.testng.Assert;
import org.testng.annotations.Test;

public class Class1 {

    FunctionsClass functionsClass = new FunctionsClass();

    @Test(description = "This test validates summation of Two Integers", priority = 1)
    public void validateSumOfTwoNumbers(){
        int sumOfTwoNumbers = functionsClass.sumOfTwoNumber(10, 20);
        Assert.assertEquals(sumOfTwoNumbers, 30);
    }

    @Test(priority = 1)
    public void ValidateDifferenceBetweenTwoNumbers(){
       int difference =  functionsClass.subtractTwoNumber(20, 40);
        Assert.assertEquals(difference, -20);
    }

    @Test
    public void ValidateMultiplicationOfTwoNumber(){
        int multiplication =  functionsClass.multiplicationOfTwoNumber(20, 40);
        Assert.assertEquals(multiplication, 700);

    }

    @Test(dependsOnMethods = {"ValidateDifferenceBetweenTwoNumbers"}, enabled = false)
    public void ValidateDivisionOfTwoNumber(){
        int division =  functionsClass.divisionOfTwoNumber(50, 10);
        Assert.assertEquals(division, 5);
    }

}
