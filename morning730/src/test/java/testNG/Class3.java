package testNG;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Class3 {



    @Test(groups = {"UI", "sanity"})
    public void UI_Validation(){
      System.out.println("UI Validations");
    }

    @Test(groups = {"sanity", "regression"})
    public void sanity_Validations1(){
        System.out.println("sanity Validations");
    }

    @Test(groups = {"regression"})
    public void regression_Validation1(){
        System.out.println("regression Validations");
    }

    @Test(groups = {"regression"})
    public void regression_Validation2(){
        System.out.println("regression Validations");
    }

}
