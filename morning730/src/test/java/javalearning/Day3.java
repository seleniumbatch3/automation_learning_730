package javalearning;

public class Day3 {

    /**
     * Increment and Decrement Operators
     * pre increemnt ++ x
     * post Increment x ++
     *
     * Pre Decrement -- x
     * post Increment x--
     */

    int x = 100;

    void function1(){

        int y = ++x;

        System.out.println("value of ++x " + ++x);

        System.out.println("Value of X " + x);

        System.out.println("Value of Y " + y);


        ++x;

        x--;

        --x;
    }

    void function2(){

        System.out.println("value of --x " + --x);

        System.out.println("value of x-- " + x--);
    }

    void function3(){
        int  x = 100;
        x=x+1;

        byte b = 100;
        b = (byte)(b+1);

        x++;

        int y = x++ + x-- + x--;

        int z = --x

                +

                x++

                +
                x--

                +

                ++x;

        System.out.println("value of x " + z);
    }

    void function4(){
        int x= 100;

        x = x+1;
        System.out.println("value of x " + x);

        x++;
        System.out.println("value of x " + x);
    }


    /**
     * Relational Operators
     * < <=, >, >=
     */

    void function5(){
        int a = 10;
        int b = 20;
      if (a<=b){
          System.out.println("value of a" + a);
      }

    }

    /**
     * Equlaity Operators
     * == , !=
     */
    void function6(){
        int a = 100;
        int b = 200;

        if (a!=100){
            System.out.println("value of a " + a);
        }
    }


    /**
     * Assignment Operators
     *  simple Assignment Operator
     *  compound Operator
     *  +=
     *  -=
     *  *=
     *  %=
     *  /=
     *  &=
     *  |=
     *  ^=
     */

    void function7(){
        int a = 100;
        a +=100;
        a -=100;
        a *=100;
        a /=100;
        a %=100;

        int b,c, d, f;

        b=c=d=f=20;

        b += c -= d *= f /= 2;

        b += -180;

        System.out.println("value of b " + b);

    }


    public static void main (String[] a){
        Day3 day3 = new Day3();
        Day3 day31 = new Day3();
        Day3 day32 = day3;

        String value = new String("Murthy");

        String value1 = "Murthy";
        String value2 = "Murthy";

        if (value.equals(value1)){

        }
        day3.function7();



    }
}
