package javalearning;

public class Day1 {

    /**
     * Literals
     * byte
     * short
     * int
     * long
     * float
     * double
     * boolean
     * char
     *
     */

    /**
     * Integral Data Types
     * byte
     * short
     * int
     * long - 9,223,372,036,854,775,807
     */

byte byteValue = 127;
short shortValue = 32000;

 static int intValue = 234567890;

long longValue = 2345678901L;

static String value = "Heloo World";

    /**
     * Float point data types
     * float
     * double
     */

    static float floatValue = 0.001111111F;
    double doubleValue = 1234.000000;


    /**
     * boolean type values
     * false
     * true
     */

   static boolean booleanValue = false;

    /**
     * char data Type
     *
     */
   static char charValue ='@';

    /**
     * String literals
     *
     */


    String stringValue = "jfkjfjfhgf 68576475457475jf%&$&&%&^%^*^*)(&*(&*(&(*&(*&*(&l;kkjhkhk";

    public static void main (String[] args){

    System.out.println(charValue );


}

}

