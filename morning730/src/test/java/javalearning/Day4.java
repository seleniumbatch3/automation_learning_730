package javalearning;

public class Day4 {
    /**
     * Compound Operators continuation
     * &
     * |
     * ^
     */


    static void function1(){
        int  a= -8;
        int  b = -9;

        System.out.println(a^b);
    }

    /**
     * Short Circuit Operator
     * &&, ||
     */

    static void function2(){
        int a = 5;
        int b = 10;

        if (a<b && (b!=a && a>b) || (a==b)){
            System.out.println("Value of b " + b);

        }
    }

    /**
     * Conditional Operators
     * ?
     * :
     */

    static void function3(String browser){

        int number = 100;
        String v;

        String name =  (browser.equalsIgnoreCase("swathi")) ? " swathi Testing is done":

                (browser.equalsIgnoreCase("Tanuja")) ? " Tanuja Testing is done" :

                        "Testing is not done";

        System.out.println(name);

        boolean b = (number > 100)? true:false;

        int a = (number == 200)? 50 :50000;



    }


    public static void main(String[] a){
        function3("dgdgsdfg");
    }
}
