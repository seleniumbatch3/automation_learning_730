package javalearning;


import modifiers.Day10;

import java.util.Date;

public class Day5  {
    /**
     * Flow - Control Statements
     */

    /**
     * if-else
     */

    byte age = 127;

    short days = 32767;

    String mobileNumber = "404_229_0735";

    int a = 214_748_3647;




    float value = 0.00f;

    double val = 1.00;

    static void function12(){

        int a= 100;
        int b = 200;

        if (a>b && a>b ){
            int c = a+b;
            System.out.println("Value of c " + c);
        }
        else if (b==200){
            int d = b*b;
            System.out.println("Value of d " + d);
        }

        else if (b==200){
            int d = b*a;
            System.out.println("Value of b " + b);
        }
        else {
            System.out.println("Value of b " + b);
        }

        if (a==100){
            String value = "Murthy";
        }


    }

    static void function2(){
        int a =10;
        int b =20;

        if(a<b){
            System.out.println("without curly braces");
            System.out.println("After curly braces");
        }
    }

    /**
     * Switch - Case
     */


    static void function3(){
        String name = "Bhargavi";

        switch(name){
            case "Teny" :
                System.out.println("Case 1");
                break;
            case "Kiran":
                System.out.println("Case 2");
                break;
            case "Swathi":
                System.out.println("Case 3");
                break;
            default:
                System.out.println("none of the cases are matched");
        }

    }

    static void function4(){

        String nameOfTheDay = "";
        switch(nameOfTheDay){
            case "Monday":
            case "Tuesday":
            case "Wednesday":
            case "Thursday":
            case "Friday":
                System.out.println(nameOfTheDay + " is Weekday");
                break;

            case "Saturday":
            case "Sunday":
                System.out.println(nameOfTheDay + " is Weekend");
                break;

            default:
                System.out.println(nameOfTheDay +  " is not matching ");
        }

    }

    static void function5(int value){

        switch(value){
            case 10:
                System.out.println("Case 1");
                break;
            case 20:
                System.out.println("Case 2");
                break;
            case 230:
                System.out.println("Case 3");
                break;
            default:
                System.out.println("none of the cases are matched");
        }

    }
    public void function45(String name ){
        name = "Swathi";
        System.out.println("Priting values " + name);
    }

    public static void main (String... test){
        function5(100);
        Day5 day5 = new Day5();


    }
}
