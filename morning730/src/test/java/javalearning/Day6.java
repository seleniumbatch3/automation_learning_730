package javalearning;

import java.util.ArrayList;
import java.util.List;

public class Day6 {
    /**
     * for loop and for each loop
     */

    void function1(){
        int i =2;

        for ( ; i<=3 || (i>=0 && i==0) ;  ){

            System.out.println("value of i " + i);

        }

        System.out.println("value of i " + i);
    }

    void function2(){
        int i =2;

        for ( ; true;  ){


            System.out.println("after loop");
        }

    }

    void function3(){
        int i =2;

        for(;i<=10 ; ){
            System.out.println("after loop");
            for (; ; ){
            }
        }
    }

    void function4(){
        String[] names = new String[4];

        names[0] = "Bhargavi";
        names[1] = "Kiran";
        names[2] = "Swathi";
        names[3] = "Teny";

        List<String> values = new ArrayList<>();
        values.add("Bhargavi");
        values.add("Kiran");
        values.add("Swathi");
        values.add("Teny");


        for ( String name : values){
            System.out.println(name);
        }

        for (int i =0; i<values.size(); i++){
            System.out.println(" name is " + names[i]);
        }
    }

    public static void main(String... args){
        Day6 day6 = new Day6();
        day6.function4();
    }
}
