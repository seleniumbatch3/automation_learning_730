package javalearning;

import modifiers.Day9;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Day7 extends Day9 {
    /**
     * While Loop
     */

    void function12(){
        int a = 100;
        int b = 200;

        Set<String> values = new HashSet<>();
        values.add("Bhargavi");
        values.add("Kiran");
        values.add("Tanuja");


        Iterator value = values.iterator();
        while(value.hasNext()){

            System.out.println("value of a" +value.next());

        }
    }

    void  function2(){
        int a = 100;
        int b = 200;
        System.out.println(a+b);
    }

    String function3(){
        function4();
        String name = "Venkat";
        String location = "Hyderabad";

        return name + "  " + location;

    }

    String function4(String locationValues){
        String updatedString = locationValues.replace("Hyderabad", "Murthy");
        return updatedString;
    }


    String function5(int age, String name){

        if(age>=18){
            return name + " is adult" +  " age is " + age;
        }
        else if ( age >= 12 &&  age<18){
            return name + " is Teenager";
        }

        System.out.println();

        return "nothing is matching";


    }



    public static void main (String[] aaa){
        Day7 day7 = new Day7();

        System.out.println(day7.function5(20, day7.function5())
        );

        Day9 day9 = new Day9();
        day9.function1();

        day7.function4();


    }
}
