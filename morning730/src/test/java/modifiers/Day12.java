package modifiers;

import java.util.ArrayList;
import java.util.List;

public class Day12 {

    /**
     * Arrays
     * 1. Array declaration
     * 2. Array Creation
     * 3. Array Initialization
     * 4 declaration creation and intitialization
     * length vs length();
     */

    int a = 20, b =30, c=40, d = 20;

    int[] input = new int[5];

    public void function(){

        input[0] = 20;
        input[1] = 30;
        input[2] = 40;
        input[3] = 50;
        input[4] = 60;

        for (int val : input ){

            System.out.println(val);
        }
    }

    /**
     * Array Declaration
     *
     */

    static int[] integerArray;

    static int val[];

    int [] val1;

    int []val2;

    public static void main(String[] args){

        integerArray = new int[3];

        integerArray[0] = 10;
        integerArray[1] =20;
        integerArray[2] = 30;

        val = new int[5];


        Day12 day12 = new Day12();
        day12.function2();
    }


    /**
     * decalration, cretion and initilization
     */


    int [] values = {10, 20, 30, 40};

    String[][] stringArray = new String[2][5];

    public void function2(){
        stringArray[0][0] = "Venkat";
        stringArray[0][1] = "Ramana";
        stringArray[0][2] = "Murthy";
        stringArray[0][3] = "Tsnuja";
        stringArray[0][4] = "Kiran";


        stringArray[1][0] = "Bhargavi";
        stringArray[1][1] = "swathi";
        stringArray[1][2] = "GAyathri";
        stringArray[1][3] = "Raj";
        stringArray[1][4] = "Dil";


        for (  String[] val :  stringArray){

            for (String element  : val){

                System.out.println(element);
                if (element.equalsIgnoreCase("swathi")){
                    break;
                }
            }
            System.out.println("======================");
        }

        int[][][][] v = new int[1][2][3][4];

        for ( int[] [] [] values : v){
            for (int[] [] val :values){
                for(int[] text : val){
                    for(int te : text){

                    }
                }
            }
        }
    }



}
