package modifiers;

public class Day9 {

    /**
     * Access Modifiers
     * 1. public
     * 2. protected
     * 3. package level(default)
     * 4. private
     */

    /**
     * These modifiers can be applied on
     * classes
     * variables
     * constructors
     * functions
     */


    public void function1(){
        System.out.println("public function");
    }

    void function2(){
        System.out.println("Default function");
    }

    private void function3(){
        System.out.println("private function");
    }

    protected void function4(){
        System.out.println("protected function");
    }
    protected String function5(){
        return "Venkata Ramana Murthy";
    }

}
