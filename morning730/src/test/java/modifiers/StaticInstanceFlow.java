package modifiers;

import OOPs.Day14;

public class StaticInstanceFlow {

    static {
        System.out.println("First static block");
        StaticInstanceFlow staticInstanceFlow = new StaticInstanceFlow();
    }
    {
        System.out.println("First instance block");
        function1();
        function();
    }

    StaticInstanceFlow(){
super();
    }
    public StaticInstanceFlow(int a, int b){
        this();

    }

    public static void function(){

        System.out.println("First Function");
        System.out.println(name);
    }

    public void function1(){

        System.out.println("First Function");
        System.out.println(secondName);
    }
    static String name = "Testing";
    String secondName = "Tanuja";

    public static void main(String[] args){
        function();
        StaticInstanceFlow staticInstanceFlow = new StaticInstanceFlow();
        staticInstanceFlow.function1();

        Day14 day14 = new Day14();
        day14.additionOfNumbers(14, 15);
    }
}
