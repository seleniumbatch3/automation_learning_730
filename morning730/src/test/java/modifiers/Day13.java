package modifiers;

public class Day13 {

    /**
     * Static control flow
     * instance control flow
     */

    /**
     * Static Control Flow
     * 1.Identification of all Static members
     * 2. Execution of Static blocks and assignments of static variables from top to bottom
     * 3. execution of main method
     */

    /**
     * Instance Control Flow
     *
     * Always in any Java program first priority goes to Static control flow
     * in between static control flow, if we create an object or any creation of an object is there then the flow will be
     *  1. identification of instance member
     *  2. execution of instance blocks and assignment of instance variables from top to bottom
     *  3. execute the constructor
     */
    static
    {
        function2();
        System.out.println ("My First Static block");

    }

    static public String name = "Venkat";

    static public int value;

    static public void function1(){
        System.out.println ("My First Function");

    }

    public static void main(String[] aaa){
        function2();
        function1();
    }

    public static void function2(){
        System.out.println ("My second Function");
        System.out.println (a);
    }

    static int a = 100;
}
