package modifiers;

import javalearning.Day5;

final strictfp public class Day10 {
    /**
     * Modifiers for Variables
     * public protected package private
     */

    public int a = 10;

    private String name = "Venkat";

    protected boolean value = true;

     final static public double VALUE_DOUBLE = 45.000;

    /**
     * Constructors
     * public private proctected
     */

    private Day10(int a){

    }

    protected Day10(){

    }

  public Day10(boolean b){

    }

    protected Day10(String value){


    }

    final public void function45(String name ){
        name = "Venkata Ramana Murthy";
        System.out.println("Priting input " + name);

    }
    final strictfp static public void main (String[] test){

    }
}
