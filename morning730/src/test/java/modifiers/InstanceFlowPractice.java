package modifiers;

public class InstanceFlowPractice {


    {
        System.out.println("My First Instance Block");
    }

    int a = 100;
    public void function1(){
        System.out.println(secondName);

    }

    {
        function1();
    }

    String secondName="Teny";

    {
        function1();
    }

    public void function2(){
        System.out.println(secondName);
    }

    public static void main(String... args){

        InstanceFlowPractice instanceFlowPractice = new InstanceFlowPractice();
        System.out.println("Main Method");
        instanceFlowPractice.function2();
        System.out.println("After Main Method");
        InstanceFlowPractice instanceFlowPractice1 = new InstanceFlowPractice();
    }
}
