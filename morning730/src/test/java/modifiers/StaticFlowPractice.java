package modifiers;

public class StaticFlowPractice {

    public static String name;

    static
    {
        name = "Bargavi";
        System.out.print(name);
        function();
    }

    public static void function(){
        System.out.println("My Function");
        System.out.println(secondName);
    }

    public static String secondName = "Kiran";

    static
    {
        function();
    }

    public static void function2(){
        System.out.println(value);
        function();
    }

    public static void main (String... ars){
        String name="Venkat";
        System.out.println(name);
        function();
        function2();

    }

    public static int value = 200;
}
