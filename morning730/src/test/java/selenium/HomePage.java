package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomePage extends BaseClass {

    /**
     * get
     * getCurrentUrl
     * getTitle
     * findElements
     * findElement
     * getPageSource
     * close
     * quit
     * getWindowHandles
     * getWindowHandle
     * switchTo
     * navigate
     * manage
     */

    @Test
    public void testCase1(){
//    launchBrowser1("chrome");

        remoteWebDriver.get("https://www.axisbank.com");
        String currentURL = remoteWebDriver.getCurrentUrl();
        System.out.println("Current URL of page ==== " + currentURL);

        String currentPageTitle = remoteWebDriver.getTitle();
        System.out.println("Current Title Of the Page ==== " + currentPageTitle);

        String pageSource = remoteWebDriver.getPageSource();
        System.out.println("Current Page Source  ==== " + pageSource);
       remoteWebDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

//        remoteWebDriver.close();
        System.out.println("Current Page is closed");
    }

    @Test
    public void testCase2(){
//        launchBrowser1("gecko");
        remoteWebDriver.get("https://www.facebook.com");

    }

    @Test
    public void testCase3(){
//        launchBrowser1("chrome");
//        remoteWebDriver.get("https://www.axisbank.com");
//        String windowID = remoteWebDriver.getWindowHandle();
//        System.out.println("Current Window ID " + windowID);
//
//        remoteWebDriver.switchTo().window(windowID);
//        remoteWebDriver.switchTo().defaultContent();
//        remoteWebDriver.switchTo().window(windowID);
//        remoteWebDriver.switchTo().frame("");
//        remoteWebDriver.switchTo().parentFrame();
//
//        remoteWebDriver.navigate().to("https://www.axisbank.com");
//        String windowID1 = remoteWebDriver.getWindowHandle();
//        System.out.println("Current Window ID " + windowID);

        int[] v = new int[]{10, 20};
        System.out.println(Arrays.asList(v));
        List<String> values = new ArrayList<String>();
    }

}
