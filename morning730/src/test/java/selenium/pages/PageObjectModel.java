package selenium.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import selenium.BaseClass;

public class PageObjectModel extends BaseClass {
    /**
     * Identification of Object Rep/Declaration of Object repository
     */

    private @FindBy(id = "user-name") WebElement userName;
    private @FindBy(id = "password") WebElement passWord;

    /**
     * Create Associated functions
     */

    /**
     * This function enters UserName
     * @param userName
     */
    public void enterUserName(String userName){
        if(isFirstNameEditBoxAvailable())
            this.userName.sendKeys(userName);
    }

    /**
     *
     * @return
     */
    public boolean isFirstNameEditBoxAvailable(){
        try{
            if (this.userName.isDisplayed() && this.userName.isEnabled())
                return true;
        }
       catch(Exception ex){
       }
        return false;
    }

    public void enterPassWord(String passWord){
        if(isPassWordEditBoxAvailable())
            this.passWord.sendKeys(passWord);
    }

    public boolean isPassWordEditBoxAvailable(){
        if (this.passWord.isDisplayed() && this.passWord.isEnabled())
            return true;
        return false;
    }

    /**
     * Initialize objects
     */

    public static PageObjectModel getInstance(){
        return  PageFactory.initElements(remoteWebDriver,  PageObjectModel.class );
    }

}
