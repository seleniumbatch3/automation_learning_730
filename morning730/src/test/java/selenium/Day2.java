package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Day2 extends BaseClass{
    /**
     * Find Element
     * Find Elements
     *
     * 1. id
     * 2. name
     * 3. className
     * 4. LinkText
     * 5. Partial LinkText
     * 6. TagName
     * 7. xpath
     * 8.cssSelector
     *
     */

    int a =100;
    String name = "Venkat";

    public String m1(String value){
        return value;
    }

    public static String m2(){
        return "Venkat";
    }

    public void function1(String browserName){

//        launchBrowser1(browserName);
        remoteWebDriver.get("https://www.saucedemo.com/");
        WebElement userName = remoteWebDriver.findElement(By.xpath("//input[@placeholder = 'Username']"));
        userName.sendKeys("standard_user");

        WebElement passWord = remoteWebDriver.findElement(By.xpath("//input[@type = 'password']"));
        passWord.sendKeys("secret_sauce");


    }

    @Test
    public void testCase1(){
        function1("chrome");
    }
    @Test
    public void testCase2(){
//        launchBrowser1("chrome");
        remoteWebDriver.get("https://www.facebook.com/");
        WebElement webElement =remoteWebDriver.findElement(By.partialLinkText("Forgot"));
        webElement.click();
    }
}
