package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Day3 extends BaseClass{
    /**
     * xpath
     * //tagname[@attributeName = 'valueOfAnAttrbute']
     */


    //input[@placeholder = 'Username']

    /**
     * AND
     * OR
     *
     * //input[(@id ='user-name'  and @name='user-name') or @autocorrect ='off']
     *
     *  text()
     *
     *  //div[text()='Sauce Labs Backpack']
     *
     *  contains
     *
     *
     *  starts-with
     *  //input[starts-with(@id, 'user')]
     *  //a[starts-with(@href, 'https://www.facebook.com/recover/initiate')]
     */

    /**
     *  * ancestor: This axes indicates all the ancestors relative to the context node, also reaching up to the root node.
     *      * ancestor-or-self: This one indicates the context node and all the ancestors relative to the context node, and includes the root node.
     *      * attribute: This indicates the attributes of the context node. It can be represented with the “@” symbol.
     *      * child: This indicates the children of the context node.
     *      * descendent: This indicates the children, and grandchildren and their children (if any) of the context node. This does NOT indicate the Attribute and Namespace.
     *      * descendent-or-self: This indicates the context node and the children, and grandchildren and their children (if any) of the context node. This does NOT indicate the attribute and namespace.
     *      * following: This indicates all the nodes that appear after the context node in the HTML DOM structure. This does NOT indicate descendent, attribute, and namespace.
     *      * following-sibling: This one indicates all the sibling nodes (same parent as context node) that appear after the context node in the HTML DOM structure. This does NOT indicate descendent, attribute, and namespace.
     *      * namespace: This indicates all the namespace nodes of the context node.
     *      * parent: This indicates the parent of the context node.
     *      * preceding: This indicates all the nodes that appear before the context node in the HTML DOM structure. This does NOT indicate descendent, attribute, and namespace.
     *      * preceding-sibling: This one indicates all the sibling nodes (same parent as context node) that appear before the context node in the HTML DOM structure. This does NOT indicate descendent, attribute and namespace.
     *      * self: This one indicates the context node.
     *
     */

    //div[text()='Sauce Labs Onesie']/ancestor::div[@class ='inventory_item_label']/following-sibling::div[@class ='pricebar']/button


    public void selectAProduct(String productType){
        launchBrowser();
        remoteWebDriver.get("https://www.saucedemo.com/");

        WebElement userName = remoteWebDriver.findElement(By.id("user-name"));
        userName.sendKeys("standard_user");

        WebElement passWord = remoteWebDriver.findElement(By.xpath("//input[@type = 'password']"));
        passWord.sendKeys("secret_sauce");

        WebElement loginButton = remoteWebDriver.findElement(By.xpath("//input[@id='login-button']"));
        loginButton.click();

        WebElement addToCart =  remoteWebDriver.findElement(By.xpath("//div[text()='" +productType+ "']/ancestor::div[@class ='inventory_item_label']/following-sibling::div[@class ='pricebar']/button"));
        addToCart.click();


    }

    @Test
    public void testCase(){
        selectAProduct("Sauce Labs Bike Light");
    }
}


