package selenium;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;

public class BaseClass {
    public static RemoteWebDriver remoteWebDriver = null;

    public void setDriverPath(String browserName){
        System.setProperty("webdriver." + browserName+ ".driver", "drivers/"+ browserName+"driver.exe");
    }


    @BeforeClass
    public RemoteWebDriver launchBrowser(){
        setDriverPath("chrome");
        switch("chrome"){
            case "chrome":
                remoteWebDriver = new ChromeDriver();
                break;
            case "gecko":
               remoteWebDriver= new FirefoxDriver();
                break;
            case "edge":
                remoteWebDriver =new EdgeDriver();
                break;
            default:
                return remoteWebDriver;
        }
        return remoteWebDriver;
    }
}
