package selenium;

import org.omg.CORBA.INTERNAL;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.ErrorHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import javax.swing.*;
import java.rmi.Remote;
import java.util.*;

abstract public class Day1 {

    RemoteWebDriver remoteWebDriver = null;
//    RemoteWebDriver remoteWebDriver1 = new ChromeDriver();
//
//    ChromeDriver chromeDriver = new ChromeDriver();

    public void function1() {
//        remoteWebDriver1.getErrorHandler();

    }


    public void launchBrowser() {

        System.setProperty("webdriver.chrome.driver", "C:\\Automation Batches\\Morning 7 30 AM\\automation_learning_730\\morning730\\drivers\\chromedriver.exe");

        ChromeDriver chromeDriver = new ChromeDriver();

        chromeDriver.get("https://www.Axisbank.com");
        String title = chromeDriver.getTitle();
        System.out.println("title of the page " + title);

    }

    public void setDriverPath(String browserName){
        System.setProperty("webdriver." + browserName+ ".driver", "drivers/"+ browserName+"driver.exe");
    }


    public RemoteWebDriver launchBrowser1(String browserName){
        setDriverPath(browserName);
        switch(browserName){
            case "chrome":
                remoteWebDriver = new ChromeDriver();
                break;
            case "firefox":
                remoteWebDriver= new FirefoxDriver();
                break;
            case "edge":
                remoteWebDriver =new EdgeDriver();
                break;
            default:
                return remoteWebDriver;
        }
        return remoteWebDriver;
    }

    @Test
    public void testCase1(){
        launchBrowser1("chrome");
    }

}

