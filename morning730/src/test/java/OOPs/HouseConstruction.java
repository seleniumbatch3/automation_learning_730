package OOPs;

public interface HouseConstruction {


   void kitchenDesign();

   void hallDesign();

   void bedRoomDesign();

   void restRoomsDesign();
}
