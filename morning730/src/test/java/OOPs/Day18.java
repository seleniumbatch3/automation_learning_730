package OOPs;

public class Day18 extends FrameWork{
    /**
     * Parent Class Constructor
     *
     * By default parent constructor will be called from current class constructor
     * super() represents parent class constructor
     * super() must be the first call
     * we cant have both super() and this() together.
     *
     * super represent parent class object
     *
     */


    public Day18( ){

    }

    public Day18(int a){

    }

    public Day18(String webDriver){

        webDriver = webDriver;
        System.out.println("Current class constructor");

    }

    public void function1(){
        System.out.println("Current class function");
    }

    public static void main (String[] test){
        Day18 day18 = new Day18();
        day18.function1();
        FrameWork frameWork = new Day18();
        ((Day18) frameWork).function1();
    }
}
