package OOPs;

public class Day15 extends Day14 {
    /**
     * Function name must be same with different arguments
     * Method overloading does not look for either modifiers or return type
     *
     */

    public static void function1(){

    }

    public static void function1(String name, int value){

    }

    private static int function1(int value){

        return 5;
    }

    public static void function1(int value, int name){

    }

    public static int addition(int a, int b)
    {
        return a+b;
    }

    public static int addition(int a, int b, int c)
    {
        return a+b+c;
    }


    public static void function2(double a){

    }

    public static void function2(float a){

    }

    public static void function3(String value){

    }

//    public static void function3(StringBuffer value){
//
//    }

    public static void function3(Object value){

    }

    public static void function4(Day15 day15){

    }

    public static void function4(Day14 day14){
        day14.setApplicationInformation();
        System.out.print(day14.getFirstName());
    }

    public static void main(String test){
        function4(new Day14());
    }

    public static void main(String[] test){
        function4(new Day14());
    }
}
