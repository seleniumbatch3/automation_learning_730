package OOPs;

public class Day17 extends Commons{
    int[] a ;
    /**
     * Constructors
     * 1. Constructor must be with Class Name only
     * 2. Constructors will not have any return type
     * 3. Constructors will accept modifiers only public private protected and package
     * 4. Default Constructor and programmer created constructor (Two types of Constructors)
     * 5. Constructors Can be overloaded
     * 6. Constructors are used to initialize objects
     * 7. This keyword represents current class object, using this keyword all instance members can be called without creating an object with in a class.
     * 6. this() represents current class constructors. this must be the first call inthe constructor body.
     */

    public String name;
    public int age;

    String s = "Murthy";
    String s3 = "Murthy";
    String s1 = new String("Murthy");
    String s2 = new String("Murthy");

    Day17(){

    }

    public Day17(String name, int age){

        this.name = name;
        this.age = age;
    }

    public void function(){
        if(true)
            a =new int[5];

        System.out.println(s==s3);
        System.out.println(Day17.class.getClassLoader());

    }

    public void function1(){
        name = "";
        function();

    }


    public static void main (String[] args){
        Day17 day17 = new Day17("Teny ", 30);
        System.out.println("day17 object1 values " + day17.name);
        System.out.println("day17 object values" + day17.age);

        Day17 day = new Day17("KIran", 20);
        System.out.println("day17 object2 values " + day.name);
        System.out.println("day17 object2 values " + day.age);
        day17.function();


    }

}
