package OOPs;

public class Day16 extends Commons{

    int age =50;
    /**
     * Constructor Overloading
     */

    public Day16(){

    }

    public Day16(String name){

    }



    /**
     * Inheritance
     * 1. Multilevel Inheritance
     * 2. Multiple Inheritance (Not applicable)
     */


    public void function1(){
        launchBrowser();
        System.out.print(super.age);
        clickOnContinueButton();
        clickOnBackButton();
        clickOnCancelButton();
    }

    /**
     * Overriding
     */
    public void function2(){
        houseProperty();
    }

    public void houseProperty2(){
        System.out.println("Windows count " + 3);
        System.out.println("Doors count " + 2);
        System.out.println("Bedrooms count " + 5);
        System.out.println("KItchen count " + 5);

    }


    public void function3(int name){

        System.out.println(name);
    }

//    private void function4(){
//        System.out.println("Private Function");
//    }

    public void function6(){

    }

    public void function7(){

    }



    public String function8(){
        function11();

        return new String("Test");
    }

    public Day16 function9(){
        return new Day16();
    }

    public static void function10(){
        System.out.println("Final Function");
    }

    public static void main(String[] test){

        Day16 day16 = new Day16();

        day16.houseProperty();

        day16.houseProperty1();

        Commons commons = new Day16();
        commons.houseProperty();


    }

}
