package OOPs;

public class Commons extends FrameWork{

    int age =25;

    public Commons(){
        System.out.println("Parent Constructor");
    }

    public void clickOnContinueButton(){
        System.out.println("Click On Continue Button");
    }

    public void clickOnCancelButton(){
        System.out.println("Click On Continue Button");
    }

    public void clickOnBackButton(){
        System.out.println("Click On Continue Button");
    }

    public void houseProperty(){
        System.out.println("Windows count " + 4);
        System.out.println("Doors count " + 5);
        System.out.println("Bedrooms count " + 4);
    }

    public void houseProperty1(){
        System.out.println("Windows count " + 4);
        System.out.println("Doors count " + 5);
        System.out.println("Bedrooms count " + 4);
    }

    public void function3(String name){

        System.out.println(name);
    }

    private void function4(){
        System.out.println("Private Function");
    }

    final public void function5(){
        System.out.println("Final Function");
    }

    void function6(){

    }

    public Object function8(){

        return new Object();
    }

   protected void function7(){

    }

    public Commons function9(){
        return new Commons();
    }

    public static void function10(){
        System.out.println("Final Function");
    }

    public void function11(){
        function4();
    }
}
