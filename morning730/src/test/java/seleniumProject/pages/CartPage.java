package seleniumProject.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import seleniumProject.utils.BaseClass;

import java.util.ArrayList;
import java.util.List;

public class CartPage extends BaseClass {

    private static CartPage cartPage;
    @FindBy(xpath = "//div[@class ='cart_item']//a/div")
    private List<WebElement> selectedProducts;

    /**
     *
     * @param webDriver
     * @return
     */
    public static CartPage getInstance(WebDriver webDriver){
        cartPage = PageFactory.initElements(webDriver, CartPage.class);
        return cartPage;
    }

    /**
     * This function returns selected items from the cart page
     * @return
     */
    public List<String> getListOfSelectedItems(){

        List<String> selectedItems = new ArrayList<>();
        for(WebElement selectedProduct:  selectedProducts){
            selectedItems.add(selectedProduct.getText());
        }
        return selectedItems;
    }
}
