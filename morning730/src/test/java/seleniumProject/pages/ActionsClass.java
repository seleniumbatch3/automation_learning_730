package seleniumProject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import seleniumProject.utils.BaseClass;

import java.util.List;

public class ActionsClass extends BaseClass {

    private static ActionsClass actionsClass = null;
    @FindBy(xpath = "//a[text() = 'Explore Products']")
    private WebElement companyLink;

    @FindBy(linkText = "Sanction Policy Statement")
    private WebElement sanctionPolicyStatement;


    public static ActionsClass getInstance( WebDriver webDriver){
        actionsClass = PageFactory.initElements(webDriver, ActionsClass.class);
        return actionsClass;
    }

    public void clickOnCompany(){
        Actions actions = new Actions(webDriver);
        actions.moveToElement(sanctionPolicyStatement).build().perform();
        actions.contextClick(sanctionPolicyStatement).build().perform();

    }

    public void getInformation(){
       List<WebElement> textInormation =  companyLink.findElements(By.xpath("./following-sibling::div/div[2]//li/a"));
       for( WebElement text: textInormation){
           System.out.println(text.getText());
       }

    }
}
