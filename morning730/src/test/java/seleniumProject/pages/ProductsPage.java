package seleniumProject.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import seleniumProject.utils.BaseClass;
import seleniumProject.utils.TakingScreenshot;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ProductsPage extends BaseClass{
    public static ProductsPage productsPage;
    public static final Logger logger = LogManager.getLogger(ProductsPage.class);
    private String xpath = "//div[contains(text(), '%s')]" +
            "/ancestor::div[@class='inventory_item_label']" +
            "/following-sibling::div";

    @FindBy(xpath = "//span[@class='title']")
    private WebElement pageHeader;

    @FindBy(xpath = "//span[@class='shopping_cart_badge']")
    private WebElement cartCount;

    @FindBy(xpath = "//a[@class='shopping_cart_link']")
    private WebElement cartLink;

    @FindBy(xpath = "//select[@class='product_sort_container']")
    private WebElement dropDown;

    @FindBy(xpath = "//div[@class='inventory_list']/div/div[2]//div[@class='inventory_item_name']")
    private List<WebElement> allProductsOnScreen;

    /**
     *
     * @param webDriver
     * @return
     */
    public static ProductsPage getInstance(WebDriver webDriver){
        productsPage = PageFactory.initElements(webDriver, ProductsPage.class);
        return productsPage;
    }

    /**
     *
     * @return
     */
    public String getPageHeader(){
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(pageHeader));

        logger.info("");
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(webDriver);
        fluentWait.withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.visibilityOf(pageHeader));

        String pageHeader = this.pageHeader.getText();
        return pageHeader;

    }

    /**
     *
     * @return
     */
    public int cartCount(){
        return Integer.parseInt(cartCount.getText());
    }

    /**
     *
     * @param productName
     */
    public String selectAProduct(String productName){

        WebElement webElement = webDriver.findElement(By.xpath(String.format(xpath, productName)))
                .findElement(By.xpath("./button[contains(text(), 'Add to cart')]"));

        String priceOfProduct = webElement.findElement(By.xpath("./preceding-sibling::div")).getText();
//        webElement.click();
        return priceOfProduct;
    }

    /**
     * This function returns all products Names available on Screen
     * @return
     */
    public List<String> getAllProducts(){
        List<String> allItems = new ArrayList<>();
        for (WebElement products : allProductsOnScreen)
            allItems.add(products.getText());
        return allItems;
    }

    /**
     * This function returns prices of all the products on application
     * @param productNames
     * @return
     */
    public List<Double> getPricesOfProduct(List<String> productNames ) {
        List<Double> prices = new ArrayList<>();

        for( String productName : productNames){
            prices.add(Double.parseDouble(selectAProduct(productName).replace("$", "")));
        }
        return prices;
    }


    public boolean displayOfRemoveButton(String productName){
        WebElement webElement = webDriver.findElement(By.xpath(String.format(xpath, productName)))
                .findElement(By.xpath("./button[contains(text(), 'Remove')]"));

        if(webElement.isDisplayed())
            return true;

        return false;
    }

    public void clickOnCartLink(){
        cartLink.click();
    }


    /**
     * This function selects dropdown Option
     * @param option
     */
    public void selectDropDownFilter(String option){
        dropDown.sendKeys(option);
    }

    public void selectDropDownFilter() throws Exception{

        List<String> dropDownOptionsByText = new ArrayList<>();
        Select select = new Select(dropDown);
        select.selectByVisibleText("Price (low to high)");
        logger.info("Dropdown value selected");
        TakingScreenshot.captureScreen();
//        select.selectByValue("hilo");
//        select.selectByIndex(2);
//       List<WebElement> options =  select.getOptions();
//
//       for (WebElement option: options){
//           dropDownOptionsByText.add(option.getText());
//       }
//
//       System.out.println(select.getWrappedElement().getText());
//    }

    }

}
