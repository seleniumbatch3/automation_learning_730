package seleniumProject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import seleniumProject.utils.BaseClass;

import java.util.ArrayList;
import java.util.List;

public class WebTables extends BaseClass {
    private static WebTables webTables;

    @FindBy(xpath = "//table[@id='customers']/tbody/tr[1]/th[contains(text(), 'Company')]/parent::tr/following-sibling::tr/td[2]")
    private List<WebElement> webTableContent;

    public String xpath_ToSelectCheckBox = "//table[@id='customers']//tr/td[2][text()='%s']/preceding-sibling::td";

    public String xpath_Headers = "//table[@id='customers']/tbody/tr[1]/th";
    public String xPath_TableContent =
    "//table[@id='customers']/tbody/tr[1]/th[contains(text(), 'Company')]/parent::tr/following-sibling::tr";

    public static WebTables getInstance(WebDriver webDriver){
        webTables = PageFactory.initElements(webDriver, WebTables.class);
        return webTables;
    }

    public List<String> getCompaniesList(){
        List<String> countryList = new ArrayList<>();
        for (WebElement country: webTableContent){
            countryList.add(country.getText());
        }
        return countryList;
    }

    /**
     * This returns required information based on inputType parameter
     * @return
     */
    public List<String> getContent(String inputType){
        List<String> headersInformation = new ArrayList<>();
        List<String> tableContentInformation = new ArrayList<>();
        List<WebElement> headers = webDriver.findElements(By.xpath(xpath_Headers));
        for ( WebElement header : headers){
            headersInformation.add(header.getText());
        }
        int i = headersInformation.indexOf(inputType);
        i +=1;

        List<WebElement> listOfItems = webDriver.findElements(By.xpath(String.format(xPath_TableContent, inputType)));

        for ( WebElement item: listOfItems){
            for( WebElement information :item.findElements(By.xpath("./td[" + i + "]"))){
                tableContentInformation.add(information.getText());
            }
        }
        return tableContentInformation;
    }

    public String selectCheckBoxForASpecificCompany(String companyName){
        WebElement webElement = webDriver.findElement(By.xpath(String.format(xpath_ToSelectCheckBox, companyName)));
        webElement.findElement(By.xpath("./input")).click();
        return webElement.findElement(By.xpath("./following-sibling::td[2]")).getText();
    }
}
