package seleniumProject.utils;

        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.chrome.ChromeDriver;
        import org.openqa.selenium.chrome.ChromeOptions;
        import org.openqa.selenium.edge.EdgeDriver;
        import org.openqa.selenium.firefox.FirefoxDriver;
        import org.openqa.selenium.opera.OperaDriver;
        import org.testng.annotations.BeforeClass;
        import org.testng.annotations.BeforeMethod;
        import org.testng.annotations.Parameters;
        import org.testng.annotations.Test;
        import seleniumProject.pages.ProductsPage;

        import java.io.FileInputStream;
        import java.io.InputStream;
        import java.util.Properties;
        import java.util.concurrent.TimeUnit;

public class BaseClass {

    public static WebDriver webDriver=null;
    public ChromeOptions chromeOptions = null;

    /**
     * Thsi function controls the driver settings
     * @return
     */
    private ChromeOptions chromeOptions(){
        chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("incognito", "start-maximized");
        return chromeOptions;
    }

    /**
     * this functions sets the driver path
     */
    private void setDriverPath(String driverName){
        if(driverName.equals("firefox")){
            driverName = "gecko";
        }
        System.setProperty("webdriver."+driverName+".driver", "drivers/"+driverName+"driver.exe");
    }


    /**
     * This function launches a browser
     * @param browserName
     * @return
     */
    @BeforeMethod
    @Parameters({"browserName"})
    public WebDriver openBrowser(String browserName){
        setDriverPath(browserName);
        switch(browserName){
            case "chrome":
                webDriver = new ChromeDriver(chromeOptions());
                break;
            case "firefox":
                webDriver = new FirefoxDriver();
                break;
            case "opera":
                webDriver = new OperaDriver();
                break;
            case "edge":
                webDriver = new EdgeDriver();
                break;
            default:
                return webDriver;
        }
        return webDriver;
    }

    public String getProperty(String key){
        Properties properties = new Properties();
        try{
            properties.load(new FileInputStream("src/test/resources/testConfig.properties"));
            return properties.getProperty(key);
        }catch (Exception e){

        }
        return null;
    }


    public static WebDriver getDriver(){
//        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return webDriver;

    }
}
