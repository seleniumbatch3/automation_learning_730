package seleniumProject.utils;

public class ElementNotAvailableOnScreenException extends RuntimeException{

    public ElementNotAvailableOnScreenException(String exceptionMessage){
        super(exceptionMessage);
    }
}
