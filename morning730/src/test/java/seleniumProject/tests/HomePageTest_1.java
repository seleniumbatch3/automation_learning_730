package seleniumProject.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.TestInstance;
import seleniumProject.data.Constants;
import seleniumProject.pages.CartPage;
import seleniumProject.pages.HomePage;
import seleniumProject.pages.ProductsPage;
import seleniumProject.utils.BaseClass;

public class HomePageTest_1 extends BaseClass {

    public WebDriver webDriver;
    HomePage homePage;
    String url;

    @BeforeClass
    public void getConfig(){
        url = getProperty("url_qa");
        homePage= HomePage.getInstance(webDriver);
    }

    @BeforeMethod
    public void initialization(){
        webDriver = getDriver();
    }


    public void validatePageTitle(){
        webDriver.get(url);
        Assert.assertEquals(webDriver.getTitle(), Constants.PAGETITLE_HOMEPAGE);
        homePage.enterUserName("")
                .enterPassWord("");
    }


    public String restoreString(String s, int[] indices) {

        char[] value = new char[s.length()];

        for (int i =0; i<indices.length; i++){
            value[indices[i]] = s.charAt(i);
        }
        return String.valueOf(value);
    }


    public String sortSentence(String s) {

        String[] splitValue = s.split(" ");
        String afterRemovingNumbers ="";
        int[] numbers = new int[splitValue.length];
        for(int i =0; i<splitValue.length; i++){
            String numberString = splitValue[i].substring(splitValue[i].length()-1);
            numbers[i] = Integer.parseInt(numberString);
            splitValue[i] = splitValue[i].replace(numberString, "");
        }
        String[] c = new String[splitValue.length+1];
        c[0] ="";
        for (int i =0; i<splitValue.length;i++){
            c[numbers[i]]= splitValue[i];
        }
        for(String val : c){
            afterRemovingNumbers= afterRemovingNumbers+val+" ";
        }
        System.out.println(afterRemovingNumbers.trim());
        return afterRemovingNumbers.trim();
    }
    @Test
    public void testCase(){
        sortSentence("two2 One1 five5 four4 three3 six6 nine9 eight8 seven7");
    }
}
