package seleniumProject.tests;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import seleniumProject.data.Constants;
import seleniumProject.pages.CartPage;
import seleniumProject.pages.HomePage;
import seleniumProject.pages.ProductsPage;
import seleniumProject.utils.BaseClass;
import seleniumProject.utils.TakingScreenshot;

import java.util.*;



public class HomePageTest extends BaseClass {
    public static final Logger logger = LogManager.getLogger(HomePageTest.class);
    public WebDriver webDriver;
    HomePage homePage;
    ProductsPage productsPage;
    CartPage cartPage;
    String url;

    @BeforeClass
    public void getConfig(){
        url = getProperty("url_qa");
    }

    @BeforeMethod
    public void initialization(){
        webDriver = getDriver();
        homePage= HomePage.getInstance(webDriver);
        productsPage = ProductsPage.getInstance(webDriver);
        cartPage= CartPage.getInstance(webDriver);

    }

    @Test
    public void enterCredentials1() throws Exception{
        webDriver.get(url);
        homePage
                .enterUserName("standard_user")
                .enterPassWord("secret_sauce")
                .clickOnLoginButton();
        String pageHeader = productsPage.getPageHeader();
        Assert.assertEquals(pageHeader, "Products".toUpperCase());

        /**
         * Sauce Labs Bolt T-Shirt
         * Sauce Labs Backpack
         * Sauce Labs Fleece Jacket
         */

        List<String> products = new ArrayList<>();
        Map<String, String> priceOfProduct = new HashMap<>();
        products.add("Sauce Labs Bolt T-Shirt");
        products.add("Sauce Labs Backpack");
        products.add("Sauce Labs Fleece Jacket");

        for ( String product: products){
            String priceValue =  productsPage.selectAProduct(product);
            priceOfProduct.put(product, priceValue);

        }
        System.out.println("Value of Sauce Labs Bolt T-Shirt ==== " + priceOfProduct);
        Assert.assertEquals(priceOfProduct.get("Sauce Labs Fleece Jacket"), "$49.99");

        int selectedItemsCount = productsPage.cartCount();
        System.out.println("Selected Products count " + selectedItemsCount);
        Assert.assertEquals(products.size(), selectedItemsCount);

        for (String product:products){
            Assert.assertTrue(productsPage.displayOfRemoveButton(product));
        }
        productsPage.clickOnCartLink();

        int sizeOfSelectedItems = cartPage.getListOfSelectedItems().size();
        System.out.println(" Selected Items from Cart screen " + cartPage.getListOfSelectedItems());

        Assert.assertEquals(products.size(), sizeOfSelectedItems);
        for (int i =0; i<products.size(); i++){
            System.out.println("our input parameter to select items " + products.get(i));
            System.out.println("our selected items on Cart Screen  " + cartPage.getListOfSelectedItems().get(i));
            System.out.println("==============================");
            Assert.assertEquals(products.get(i), cartPage.getListOfSelectedItems().get(i));
        }
    }


    @Test
    public void selectDropDownValues() throws Exception{
        logger.info("selectDropDownValues  test started" );
        webDriver.get("https://www.saucedemo.com/");
        homePage
                .enterUserName("standard_user")
                .enterPassWord("secret_sauce")
                .clickOnLoginButton();

        productsPage.selectDropDownFilter();
        logger.info("selectDropDownValues  test ended" );
    }

    @Test
    public void validateFilteringOption() throws Exception{


        webDriver.get(url);
        homePage
                .enterUserName("standard_user")
                .enterPassWord("secret_sauce")
                .clickOnLoginButton();
        List<Double> expectedValues = productsPage.getPricesOfProduct(productsPage.getAllProducts());
        Collections.sort(expectedValues);

        productsPage.selectDropDownFilter();

        List<Double> actualValues = productsPage.getPricesOfProduct(productsPage.getAllProducts());

        Assert.assertEquals(expectedValues.size(), actualValues.size());
        for(int i =0; i<expectedValues.size(); i++){
            System.out.println("expectdValue is " + expectedValues.get(i) + " ==== actualValue " + actualValues.get(i) );
            Assert.assertEquals(expectedValues.get(i), actualValues.get(i));
        }


    }

    //    @AfterMethod
    public void closeBrowser(){
        webDriver.close();
        System.out.println("Closing browser");
    }
}
