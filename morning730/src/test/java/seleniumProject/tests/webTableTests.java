package seleniumProject.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import seleniumProject.pages.WebTables;
import seleniumProject.utils.BaseClass;

public class webTableTests extends BaseClass {

    private String url;
    private WebDriver webDriver;
    private WebTables webTables;

    @BeforeClass
    public void getConfig(){
        url = getProperty("url_table");
    }

    @BeforeMethod
    public void initialization(){
        webDriver = getDriver();
        webTables = WebTables.getInstance(webDriver);
    }

    @Test
    public void validateCountryList(){
        webDriver.get(url);

//        for( String companyName : webTables.getContent("Company")){
//            System.out.println("Owner of " + companyName + " ===== " + webTables.selectCheckBoxForASpecificCompany(companyName));
//        }
    }

    @DataProvider(name = "Testing")
    public Object[][] dataProvider(){

        Object[][] values = new Object[2][3];
        values[0][0] = "Murthy";
        values[0][1] = "Testing";
        values[0][2] = "Supraja";

        values[1][0] = "Sai";
        values[1][1] = "Siva";
        values[1][2] = "Kiran";

        return values;
    }

   @Test(dataProvider = "Testing")
    public void testCase1(String a, String b, String c){

       System.out.println("First Value " + a);
       System.out.println("First Value " + b);
       System.out.println("First Value " + c);
    }
}
