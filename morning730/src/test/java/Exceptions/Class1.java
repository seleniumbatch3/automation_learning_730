package Exceptions;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Class1 {
    /**
     *
     * Checked vs UnChecked
     * final finally finalize
     * throw and throws
     */

    @Test
    public void m1(){
        int a=0;
        int b = 10;
        System.out.println(b/a);
    }


    public void m2() throws InterruptedException{
        Thread.sleep(5000);
    }

    public void m3()  throws InterruptedException{
        m2();
    }


    public void checkignFileAvailablity()  {

        NullPointerException nullPointerException;
        ArithmeticException arithmeticException;
        ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException;

        try{
            FileInputStream fileInputStream =
                    new FileInputStream("C:\\Automation Batches\\Morning 7 30 AM\\automation_learning_730\\morning730\\drivers\\sting.txt");
        }
        catch(Exception error){
            System.out.println("Specified file is not found at the given location so you can use another alternative file or path ");

        }finally{

        }

    }

    @Test
    public void m4(){
        checkignFileAvailablity();
        int a = 10;
        int b = 0;

//        System.out.println(a/b);
//
//        String s = "Value";
//        System.out.println(s);

    }

    @Test
    public void m5() {

//        try{
//            FileInputStream fileInputStream =
//                    new FileInputStream("drivers/Tsting.txt");
//        }
//        finally{
//
//        }



//        int a = 10;
//        int b = 0;
//
//        System.out.println(a/b);
//
//        String s = "Value";
//        System.out.println(s);

    }

    @Test
    public void m6(){

        String[] values = new String[5];
        values[0] = "ABC";
        values[1] = "ABC";
        values[2] = "ABC";
        values[3] = "ABC";
        values[4] = "ABC";
        values[5] = "ABC";
    }

    @Test
    public void m7(){

        String originalValue = "34567u567345345645ggdfg3453465";
        int totalCount =0;
        String[] afterValue = originalValue.split("");

        for(String a  : afterValue){
            try{
                totalCount= totalCount+ Integer.parseInt(a);
            }
            catch(Error exceptionMessage){
                exceptionMessage.printStackTrace();
                System.out.println(exceptionMessage.getMessage());

            }
        }



        System.out.println("Total Count " + totalCount);

//        String[] values = new String[5];
//        values[0] = "ABC";
//        values[1] = "ABC";
//        values[2] = "ABC";
//        values[3] = "ABC";
//        values[4] = "ABC";
//        values[5] = "ABC";
    }


    public int getDivisionNumber(int a, int b ){
        int quotient = 0;
        try{
            quotient = a/b;
            return quotient;
        }catch (Exception exceptionMessage){
            System.out.println(exceptionMessage);

        }
        return quotient;
    }

    @Test
    public void testCase1(){
        Class1 class1 = new Class1();
        System.out.println(class1.getDivisionNumber(5000, -11));
    }
}
