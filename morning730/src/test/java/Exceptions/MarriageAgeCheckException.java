package Exceptions;

public class MarriageAgeCheckException extends RuntimeException{

    public MarriageAgeCheckException(String exceptionMessage){
        super(exceptionMessage);
    }
}
