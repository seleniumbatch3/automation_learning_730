package Exceptions;

import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Class3 {
    /**
     * Checked Exception
     */

    String name = "Venkat";
    public Class3() throws Exception{

    }
    @Test
    public void function1() {

        try
        {
            Class.forName("Class3");

        }
        catch(ClassNotFoundException exception)
        {
            System.out.println("Class is not avialble");
        }

    }


    public void function() throws InterruptedException{

      Thread.sleep(555);
    }

    public void function3() throws InterruptedException, FileNotFoundException, NullPointerException{
        function();
        FileInputStream fileInputStream = new FileInputStream("");
    }

    @Test
    public void function4(){
        System.out.println(10/0);
    }

    public void checkAgeForMarriage(String gender, int age){
        if(gender.equalsIgnoreCase("female") && age<18){
            throw new MarriageAgeCheckException("You are not eligible for Marriage, Please wait for sometime");
        }
        else if(gender.equalsIgnoreCase("male") && age<21){
            throw new MarriageAgeCheckException("You are not eligible for Marriage, Please wait for sometime");

        }
        else{
            System.out.println("you are eligible for marriage");
        }
    }

    @Test
    public void testCase1(){
        checkAgeForMarriage("male", 20);
    }
}
