package Exceptions;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.safari.ConnectionClosedException;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Class2 {
    /**
     * try
     * catch
     * finally
     * throw
     * throws
     */


    @Test
    public void m1(){
        int[] numbers = new int[10];

        for (int i =0; i<=10; i++){

            try{
                numbers[i] =i;
            }
            catch(ArrayIndexOutOfBoundsException exception){
                int number = i+1;
                System.out.println("Size of numbers array is " + numbers.length + " so " + number + " element does not fit into numbers array");
            }
            finally{
                System.out.println("Finally Block");
            }


        }
    }


    @Test
    public void m2(){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(null);
        arrayList.add(10);
        arrayList.add(20);
        Map<String, Integer> value = new HashMap<>();
        value.put("Testing", 10);
        System.out.println(value.get("Murthy"));

        int[] values = new int[4];
System.out.println(values[10]);

        for(int i =0; i<=4; i++){
            try{
                values[i] = arrayList.get(i);
            }
            catch(NullPointerException exception){
                System.out.println(exception.getMessage());
                System.out.println(arrayList.get(i) + " is not accepted by values array");

            }catch(IndexOutOfBoundsException exception){
                int number = i+1;
                System.out.println("Size of numbers array is " + values.length + " so " + number + " element does not fit into numbers array");
            }
            catch(NumberFormatException exception){

            }
            catch(ArithmeticException a){

            }

            catch(Exception exception){

                System.out.println("Message");
            }


        }

    }
    @Test
    public void m3(){
    }
}
