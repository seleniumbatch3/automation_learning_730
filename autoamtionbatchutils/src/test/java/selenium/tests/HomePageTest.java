package selenium.tests;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import selenium.pages.HomePage;
import selenium.pages.ProductsPage;
import selenium.utils.BaseClass;


import java.io.File;
import java.lang.reflect.Method;
import java.util.*;


public class HomePageTest extends BaseClass {
    public static final Logger logger = LogManager.getLogger(HomePageTest.class);
    public WebDriver webDriver;
    HomePage homePage;
    ProductsPage productsPage;

    String url;
    public static Map<String, String> data;

    @BeforeClass
    public void getConfig(){
        url = getProperty("url_qa");
    }

    @BeforeMethod
    public void initialization(){
        webDriver = getDriver();
        homePage= HomePage.getInstance(webDriver);
        productsPage = ProductsPage.getInstance(webDriver);
    }

    @Test(dataProvider = "byRowName")
    public void enterCredentials1(String UserName, String PassWord) throws Exception {
        extentTest = extentReports.startTest("enterCredentials1");

        webDriver.get(url);
        homePage
                .enterUserName(UserName);
        extentTest.log(LogStatus.INFO, "Enter Credentials" +
                extentTest.addScreenCapture(captureScreenshot()));
              homePage  .enterPassWord(PassWord);
        extentTest.log(LogStatus.INFO, "Enter PassWord" +
                extentTest.addScreenCapture(captureScreenshot()));
               homePage .clickOnLoginButton();
        extentTest.log(LogStatus.INFO, "Products Page is displayed" +
                extentTest.addScreenCapture(captureScreenshot()));

        Assert.assertEquals(webDriver.getTitle(), "Swag Labs");
        extentTest.log(LogStatus.PASS, "Validating Products Page Title" +
                extentTest.addScreenCapture(captureScreenshot()));
    }


    @Test(dataProvider = "byRowName")
    public void selectDropDownValues(String UserName, String PassWord) throws Exception{
        extentTest = extentReports.startTest("selectDropDownValues");

        webDriver.get(url);
        homePage
                .enterUserName(UserName);
        extentTest.log(LogStatus.INFO, "Enter Credentials" +
                extentTest.addScreenCapture(captureScreenshot()));
        homePage  .enterPassWord(PassWord);
        extentTest.log(LogStatus.INFO, "Enter PassWord" +
                extentTest.addScreenCapture(captureScreenshot()));
        homePage .clickOnLoginButton();
        extentTest.log(LogStatus.INFO, "Products Page is displayed" +
                extentTest.addScreenCapture(captureScreenshot()));

        Assert.assertEquals(webDriver.getTitle(), "Swag Labs");
        extentTest.log(LogStatus.PASS, "Validating Products Page Title" +
                extentTest.addScreenCapture(captureScreenshot()));
    }

//    @AfterMethod
    public void closeBrowser(){
        webDriver.close();
        System.out.println("Closing browser");
    }


    @DataProvider(name = "byRowName")
    public Object[][] test (Method methodName) throws Exception{
        ArrayList<Integer> rowNumbers = new ArrayList<>();
        File file = new File ("./src/test/resources/testdata/TestData.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(file);
        XSSFSheet sheet = wb.getSheet("QAT01");


        XSSFRow columnHeaders = sheet.getRow(0);
        data = new LinkedHashMap<>();


        XSSFRow rowData;
        for (int a = 0; a<sheet.getLastRowNum(); a++){
            rowData = sheet.getRow(a+1);
            if(rowData.getCell(0).getStringCellValue().equalsIgnoreCase(methodName.getName())){
                rowNumbers.add(rowData.getRowNum());
                for (int i =1; i<rowData.getLastCellNum(); i++){
                    data.put(columnHeaders.getCell(i).getStringCellValue(), rowData.getCell(i).getStringCellValue().trim());
                }
            }
        }
        Object[][] value= new Object[rowNumbers.size()][2];

        for (int j =0; j<rowNumbers.size(); j++){
            rowData = sheet.getRow(rowNumbers.get(j));
            for (int k =0; k<2; k++){
                value[j][k]= rowData.getCell(k+1).getStringCellValue();
            }
        }
        return value;
    }
}
