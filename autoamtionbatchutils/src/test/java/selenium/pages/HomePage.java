package selenium.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import selenium.utils.BaseClass;


public class HomePage extends BaseClass {
    public static final Logger logger = LogManager.getLogger(HomePage.class);

    public static HomePage homePage = null;

    @FindBy(id = "user-name")
    private WebElement userName;

    @FindBy(id = "password")
    private WebElement passWord;

    @FindBy(id = "login-button")
    private WebElement loginButton;

    public static HomePage getInstance(WebDriver webDriver){
        homePage = PageFactory.initElements(webDriver, HomePage.class);
        return homePage;
    }

    /**
     * This function takes care of entering userName
     * @param userName
     */
    public HomePage enterUserName(String userName){
        try{
            if(this.userName.isDisplayed()){
                logger.info("UserName Edit Box is displayed");
                Actions actions = new Actions(webDriver);
                actions.sendKeys(this.userName, userName).build().perform();
                logger.info("Enter user Name " + userName);

            }
        }
        catch(Exception e){
            logger.error("User Name Edit box is not available");
//            throw new ElementNotAvailableOnScreenException("Element " + this.userName + "is not avilable");
        }
        return this;
    }

    /**
     * This function takes care of entering pwd
     * @param passWord
     */
    public HomePage enterPassWord(String passWord){
        try{
            if(this.passWord.isDisplayed()){
                logger.info("PassWord Edit Box is displayed");
                this.passWord.sendKeys(passWord);
                logger.info("Enter PassWord " + passWord);
            }
        }
        catch(Exception e){
//            throw new ElementNotAvailableOnScreenException("Element " + this.passWord + "is not avilable");
        }
        return this;
    }

    /**
     * This function takes care of clicking login button
     * @return
     */
    public HomePage clickOnLoginButton() throws Exception{

        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(loginButton));
//        TakingScreenshot.captureScreen();
        try{
            if(this.loginButton.isDisplayed()){
               loginButton.click();
            }
        }
        catch(Exception e){
//            throw new ElementNotAvailableOnScreenException("Element " + this.loginButton + "is not avilable");
        }
        return this;
    }
}
