package selenium.utils;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ReadingFiles{

    public static Map<String, String> data;
    public static void dataProvider(Method methodName) throws Exception{

        File file = new File ("./src/test/resources/testdata/TestData.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(file);
        XSSFSheet sheet = wb.getSheet("QAT01");
        XSSFRow columnHeaders = sheet.getRow(0);
        data = new LinkedHashMap<>();

        for (int a = 0; a<sheet.getLastRowNum(); a++){
            XSSFRow rowData = sheet.getRow(a+1);
            if(rowData.getCell(0).getStringCellValue().equalsIgnoreCase(methodName.getName())){
                for (int i =1; i<rowData.getLastCellNum(); i++){
                    data.put(columnHeaders.getCell(i).getStringCellValue(), rowData.getCell(i).getStringCellValue().trim());
                }

            }

        }
    }

    public static Map<String, String> dataStoring(){
        return data;
    }



    public static void test (Method methodName) throws Exception{

        ArrayList<Integer> rowNumbers = new ArrayList<>();
        int count= 0;
        File file = new File ("./src/test/resources/testdata/TestData.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(file);
        XSSFSheet sheet = wb.getSheet("QAT01");
        XSSFRow columnHeaders = sheet.getRow(0);
        data = new LinkedHashMap<>();
        XSSFRow rowData;
        for (int a = 0; a<sheet.getLastRowNum(); a++){
            rowData = sheet.getRow(a+1);
            if(rowData.getCell(0).getStringCellValue().equalsIgnoreCase(methodName.getName())){
                count++;
                rowNumbers.add(rowData.getRowNum());
                for (int i =1; i<rowData.getLastCellNum(); i++){
                    data.put(columnHeaders.getCell(i).getStringCellValue(), rowData.getCell(i).getStringCellValue().trim());
                }
            }
        }
        Object[][] value= new Object[rowNumbers.size()][3];
//        int val =0;
//        for (int i = 0; i<rowNumbers.size(); i++){
//            rowData = sheet.getRow(rowNumbers.get(i));
//            val = rowData.getLastCellNum()-1;
//            value = new Object[rowNumbers.size()][val];
//        }
        for (int j =0; j<rowNumbers.size(); j++){
            rowData = sheet.getRow(rowNumbers.get(j));
            for (int k =0; k<3; k++){
                value[j][k]= rowData.getCell(k+1);
            }
        }
    }

    @DataProvider(name = "Testing")
    public Object[][] dataProvider(){

        Object[][] values = new Object[2][3];
        values[0][0] = "Murthy";
        values[0][1] = "Testing";
        values[0][2] = "Supraja";

        values[1][0] = "Sai";
        values[1][1] = "Siva";
        values[1][2] = "Kiran";

        return values;
    }

    @Test(dataProvider = "Testing")
    public void testCase1(String a, String b, String c){

        System.out.println("First Value " + a);
        System.out.println("First Value " + b);
        System.out.println("First Value " + c);
    }
}
