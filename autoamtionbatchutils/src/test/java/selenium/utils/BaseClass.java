package selenium.utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import java.net.URL;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class BaseClass {

    public static WebDriver webDriver=null;
    public ChromeOptions chromeOptions = null;
    protected ExtentReports extentReports;
    protected ExtentTest extentTest;
    private static Timestamp timestamp;
    /**
     * Thsi function controls the driver settings
     * @return
     */
    private ChromeOptions chromeOptions(){
        chromeOptions = new ChromeOptions();
//        Map<String, String> mobileEmulation = new HashMap<>();
//        mobileEmulation.put("deviceName", "iPad Pro");
//        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        chromeOptions.addArguments("incognito", "start-maximized");
        return chromeOptions;
    }



    /**
     * this functions sets the driver path
     */
    private void setDriverPath(String driverName){
        if(driverName.equals("firefox")){
            driverName = "gecko";
        }
        System.setProperty("webdriver."+driverName+".driver", "drivers/"+driverName+"driver.exe");
    }


    /**
     * This function launches a browser
     * @param browserName
     * @return
     */
    @BeforeMethod
    @Parameters({"browserName", "runLocal"})
    public WebDriver openBrowser(String browserName, Boolean runLocal) throws Exception{
        extentReports = new ExtentReports("./reports/ExtentReportResults.html", true);
        setDriverPath(browserName);
//        URL remoteUrl = new URL ("http://127.0.0.1:4444/wd/hub");
        URL remoteUrl = new URL("https://vupputuri:85dab1f1-a620-4661-a7a6-d7f7abc8048c@ondemand.us-west-1.saucelabs.com:443/wd/hub");
        switch(browserName){
            case "chrome":
                if (runLocal == true ){
                    webDriver = new ChromeDriver(chromeOptions());
                }
                else {
                    webDriver = new RemoteWebDriver(remoteUrl, chromeOptions());
                }

                break;
            case "firefox":
                webDriver = new FirefoxDriver();
                break;
            case "opera":
                webDriver = new OperaDriver();
                break;
            case "edge":
                webDriver = new EdgeDriver();
                break;
            default:
                return webDriver;
        }
        return webDriver;
    }

    @AfterMethod
    public void closeBrowser(ITestResult result, Method method) throws Exception{
        if (result.getStatus() == ITestResult.FAILURE) {
            extentTest.log(LogStatus.FAIL, "Test failed " + result.getThrowable() + extentTest.addScreenCapture(captureScreenshot()));
        } else if (result.getStatus() == ITestResult.SKIP) {
            extentTest.log(LogStatus.SKIP, "Test skipped " + result.getThrowable() + extentTest.addScreenCapture(captureScreenshot()));
        } else {
            extentTest.log(LogStatus.PASS, "Test passed" +
                    extentTest.addScreenCapture(captureScreenshot()));
        }
        extentReports.endTest(extentTest);
        extentReports.flush();
        webDriver.quit();
    }

    public String getProperty(String key){
        Properties properties = new Properties();
        try{
            properties.load(new FileInputStream("src/test/resources/testConfig.properties"));
            return properties.getProperty(key);
        }catch (Exception e){

        }
        return null;
    }


    public static WebDriver getDriver(){
//        webDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return webDriver;

    }

    public String captureScreenshot() throws Exception {
        timestamp = new Timestamp(System.currentTimeMillis());
        long fileName = timestamp.getTime();
//        Date d = new Date();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HHmmss");
//        File scrFile = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
//        FileUtils.copyFile(scrFile, new File("src/screenshots/" + sdf.format(d) + ".png"));
        File screenshot = ((TakesScreenshot)webDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("./reports/" + fileName + ".png"));
        return fileName +".png";
    }
}
